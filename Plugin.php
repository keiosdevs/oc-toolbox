<?php namespace Keios\Toolbox;

use System\Classes\PluginBase;

/**
 * toolbox Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'keios.toolbox::lang.plugin.name',
            'description' => 'keios.toolbox::lang.plugin.description',
            'author'      => 'keios',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * @return array
     */
    public function registerReportWidgets()
    {
        return [
            'Keios\Toolbox\ReportWidgets\Toolbox' => [
                'label'   => 'keios.toolbox::lang.plugin.name',
                'context' => 'dashboard'
            ]
        ];
    }
}
