<?php

return [
    'plugin' => [
        'name' => 'Keios Toolbox',
        'description' => 'Provides Report Widgets with some buttons',
        'cache_success' => 'Cache cleared succesfuly!',
        'theme_success' => 'Theme set succesfuly. Please refresh the page.',
        'currencies_success' => 'Currencies updated succesfully!',
        'optimize_success' => 'Framework optimization was done succesfully!',
        'cache_clear' => 'Clear cache',
        'red_theme' => 'Red theme',
        'blue_theme' => 'Blue theme',
        'default_theme' => 'Default theme',
    ],
];