<?php

return [
    'plugin' => [
        'name' => 'Keios Toolbox',
        'description' => 'Udostępnia widget z kilkoma przyciskami',
        'cache_success' => 'Cache został wyczyszczony.',
        'theme_success' => 'Motyw backendu został zmieniony, a cache wyczyszczony. Odśwież stronę.',
        'currencies_success' => 'Kursy walut zostały zsynchronizowane z Yahoo Finance',
        'optimize_success' => 'Optymalizacja frameworka przebiegał pomyślnie',
        'cache_clear' => 'Wyczyść Cache',
        'red_theme' => 'Czerwony Motyw Backendu',
        'blue_theme' => 'Niebieski Motyw Backendu',
        'default_theme' => 'Domyślny Motyw Backendu',
    ],
];