<?php namespace Keios\Toolbox\ReportWidgets;

use Backend\Classes\ReportWidgetBase;


/**
 * Class Toolbox
 * @package Keios\Toolbox\ReportWidgets
 */
class Toolbox extends ReportWidgetBase
{
    public $require = [
        'Keios.KeiosSeeder',
    ];

    /**
     * @var string
     */
    protected $defaultAlias = 'keios_toolbox';

    /**
     * @return mixed
     * @throws \SystemException
     */
    public function render()
    {
        $this->vars[ 'size' ] = $this->getSizes();
        $this->vars[ 'radius' ] = $this->property("radius");
        $widget = 'toolbox_widget';

        return $this->makePartial($widget);
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [
            'title' => [
                'title'             => 'backend::lang.dashboard.widget_title_label',
                'default'           => 'keios.toolbox::lang.plugin.name',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'backend::lang.dashboard.widget_title_error',
            ],
        ];
    }

    /**
     * @return array
     * @throws \SystemException
     */
    public function onCacheClear()
    {
        \Artisan::call('cache:clear');
        \Flash::success(e(trans('keios.toolbox::lang.plugin.cache_success')));
        $widget = 'toolbox_widget';

        return [
            'partial' => $this->makePartial(
                $widget,
                ['size' => $this->getSizes(), 'radius' => $this->property("radius")]
            ),
        ];
    }

    /**
     * @return array
     * @throws \SystemException
     */
    public function onSwitchToRed()
    {
        \Artisan::call('kseed:theme', ['theme' => 'red']);
        \Artisan::call('cache:clear');
        \Flash::success(e(trans('keios.toolbox::lang.plugin.theme_success')));
    }

    /**
     * @return array
     * @throws \SystemException
     */
    public function onSwitchToBlue()
    {
        \Artisan::call('kseed:theme', ['theme' => 'blue']);
        \Artisan::call('cache:clear');
        \Flash::success(e(trans('keios.toolbox::lang.plugin.theme_success')));
    }

    /**
     * @return array
     * @throws \SystemException
     */
    public function onSwitchToDefault()
    {
        \Artisan::call('kseed:theme', ['theme' => 'default']);
        \Artisan::call('cache:clear');
        \Flash::success(e(trans('keios.toolbox::lang.plugin.theme_success')));
    }

    /**
     * @return array
     * @throws \SystemException
     */
    public function onRefreshCurrencies()
    {
        \Artisan::call('pg:update-currencies');
        \Flash::success(e(trans('keios.toolbox::lang.plugin.currencies_success')));
        $widget = 'toolbox_widget';

        return [
            'partial' => $this->makePartial(
                $widget,
                ['size' => $this->getSizes(), 'radius' => $this->property("radius")]
            ),
        ];
    }

    /**
     * @return array
     * @throws \SystemException
     */
    public function onOptimize()
    {
        \Artisan::call('optimize');
        \Flash::success(e(trans('keios.toolbox::lang.plugin.optimize_success')));
        $widget = 'toolbox_widget';

        return [
            'partial' => $this->makePartial(
                $widget,
                ['size' => $this->getSizes(), 'radius' => $this->property("radius")]
            ),
        ];
    }



    /**
     * @return mixed
     */
    private function getSizes()
    {

        $s[ 'ccache_b' ] = $this->getDirectorySize(storage_path().'/cms/cache');
        $s[ 'ccache' ] = $this->formatSize($s[ 'ccache_b' ]);
        $s[ 'ccombiner_b' ] = $this->getDirectorySize(storage_path().'/cms/combiner');
        $s[ 'ccombiner' ] = $this->formatSize($s[ 'ccombiner_b' ]);
        $s[ 'ctwig_b' ] = $this->getDirectorySize(storage_path().'/cms/twig');
        $s[ 'ctwig' ] = $this->formatSize($s[ 'ctwig_b' ]);
        $s[ 'fcache_b' ] = $this->getDirectorySize(storage_path().'/framework/cache');
        $s[ 'fcache' ] = $this->formatSize($s[ 'fcache_b' ]);
        $s[ 'all' ] = $this->formatSize($s[ 'ccache_b' ] + $s[ 'ccombiner_b' ] + $s[ 'ctwig_b' ] + $s[ 'fcache_b' ]);

        return $s;
    }

    /**
     * @param $directory
     *
     * @return int
     */
    private function getDirectorySize($directory)
    {
        if (count(scandir($directory)) == 2) {
            return 0;
        }
        $size = 0;
        foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($directory)) as $file) {
            $size += $file->getSize();
        }

        return $size;
    }

    /**
     * @param $size
     *
     * @return string
     */
    private function formatSize($size)
    {
        $mod = 1024;
        $units = explode(' ', 'B KB MB GB TB PB');
        for ($i = 0; $size > $mod; $i++) {
            $size /= $mod;
        }

        return round($size, 2).' '.$units[ $i ];
    }




}
